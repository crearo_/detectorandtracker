package crearo.algo;
/**
 * Created by rish on 3/1/17.
 */

import crearo.OpenCVWindow;
import crearo.utils.Config;
import crearo.utils.Log;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.SVM;
import org.opencv.objdetect.HOGDescriptor;
import org.opencv.video.Video;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.opencv.core.CvType.CV_32F;
import static org.opencv.core.CvType.CV_64F;

public class DetectorAndTracker {

    public DetectorAndTracker(OpenCVWindow openCVWindow) {
        this.openCVWindow = openCVWindow;
    }

    private List<Detection> frameDetections = new ArrayList<>();
    private OpenCVWindow openCVWindow;
    private Scalar colorBlue = new Scalar(255, 0, 0);
    private Scalar colorGreen = new Scalar(0, 255, 0);
    private Scalar colorRed = new Scalar(0, 0, 255);
    private Size windowSize = new Size(32, 32);
    private Size blockSize = new Size(16, 16);
    private Size blockStride = new Size(8, 8);
    private Size cellSize = new Size(8, 8);
    private int bins = 9;
    private boolean hasSetupSVM = false;
    private HOGDescriptor hogDescriptor;
    private int frameIndex = 0;
    private Mat prevFrame, curFrame;
    private Rect boundingFrame;
    private int noOfNewDetections = 0;

    private void setupSvm() {
        hogDescriptor = new HOGDescriptor(windowSize, blockSize, blockStride, cellSize, bins, 1, -1, HOGDescriptor.L2Hys, 0.2, false, 12, false);
        SVM svm = SVM.load(Config.YML_FILE);
        Mat sv = svm.getSupportVectors();
        Mat alpha = new Mat();
        Mat svidx = new Mat();
        double rho = svm.getDecisionFunction(0, alpha, svidx);

        if (!(alpha.total() == 1 && svidx.total() == 1 && sv.rows() == 1)) {
            Log.d("Assertion Error");
            return;
        }
        if (!((alpha.type() == CV_64F && alpha.get(0, 0)[0] == 1.0)
                || (alpha.type() == CV_32F && alpha.get(0, 0)[0] == 1.f))) {
            Log.d("Assertion Error");
            return;
        }
        if (!(sv.type() == CV_32F)) {
            Log.d("Assertion Error");
            return;
        }

        ArrayList<Float> hogDetectorList = new ArrayList<>();
        for (int i = 0; i < sv.cols(); i++) {
            hogDetectorList.add((float) sv.get(0, i)[0]);
        }
        hogDetectorList.add((float) -rho);
        MatOfFloat hogDetector = new MatOfFloat();
        hogDetector.fromList(hogDetectorList);
        hogDescriptor.setSVMDetector(hogDetector);

        boundingFrame = new Rect(0, 0, curFrame.width(), curFrame.height());
        hasSetupSVM = true;
    }

    public void doHog(Mat matInput) {
        frameIndex++;
        curFrame = matInput;
        Imgproc.resize(curFrame, curFrame, new Size(curFrame.cols() / 2, curFrame.rows() / 2));
        detectObjects();
        matchPrevCurRects();
        drawRectsOnCurrentFrame();

        // update detections and remove those that are all tracked
        for (int i = 0; i < frameDetections.size(); i++) {
            if (frameDetections.get(i).hasBeenTrackedForHistoryLength(frameIndex))
                frameDetections.remove(i--);
        }

        prevFrame = curFrame.clone();
    }

    private void drawRectsOnCurrentFrame() {
        for (Detection detection : frameDetections) {
            if (detection.totalDetections >= 3)
                if (detection.getLatest().isDetectedFromSVM) {
                    Imgproc.rectangle(curFrame, detection.getLatest().position.tl(), detection.getLatest().position.br(), colorBlue);
                    // openCVWindow.displayMat(curFrame.submat(detection.getLatest().position), 1);
                } else {
                    Imgproc.rectangle(curFrame, detection.getLatest().position.tl(), detection.getLatest().position.br(), colorRed);
                }
        }
    }

    /**
     * Detects objects on screen and adds to detected list
     */
    private void detectObjects() {
        if (!hasSetupSVM)
            setupSvm();

        MatOfDouble findWeights = new MatOfDouble();
        MatOfRect locations = new MatOfRect();
        hogDescriptor.detectMultiScale(curFrame, locations, findWeights, 0, new Size(), new Size(), 1.1, 2.0, false);
        List<Rect> locationsList = locations.toList();

        for (int i = 0; i < locationsList.size(); i++) {
            if (findWeights.get(i, 0)[0] > 0.45) {
                Point tl = locationsList.get(i).tl();
                Point br = locationsList.get(i).br();
                Rect newRect = new Rect(tl, br);
                int id = getIdOfRect(newRect);
                Detection.Location location = new Detection.Location(frameIndex, newRect, true);
                boolean frameExists = false;
                for (Detection detection : frameDetections) {
                    if (detection.id == id) {
                        detection.addLocation(location);
                        frameExists = true;
                    }
                }
                if (!frameExists) {
                    Detection detection = new Detection(id, location);
                    frameDetections.add(detection);
                }
            }
        }
    }

    private int getIdOfRect(Rect current) {
        double percentAreaMatched = 0;
        int id = -1;
        for (Detection prev : frameDetections) {
            double commonArea = percentageOfRect1InRect2(current, prev.getLatest().position);
            if (commonArea > Config.AREA_THRESHOLD_FOR_RECT_MATCHING && commonArea > percentAreaMatched) {
                percentAreaMatched = commonArea;
                id = prev.id;
            }
        }
        if (percentAreaMatched == 0) {
            id = ++noOfNewDetections;
        }
        return id;
    }

    private void matchPrevCurRects() {
        /* Detect object that was not detected during this frame */
        for (Detection detection : frameDetections) {
            if (detection.getLatest().frameId != frameIndex) {
                trackObject(detection);
            }
        }
    }

    /**
     * @param r1
     * @param r2
     * @return Calculates "r1 is what % of r2"
     */
    private double percentageOfRect1InRect2(Rect r1, Rect r2) {
        double dx = Math.min(r1.br().x, r2.br().x) - Math.max(r1.tl().x, r2.tl().x);
        double dy = Math.min(r1.br().y, r2.br().y) - Math.max(r1.tl().y, r2.tl().y);
        if ((dx >= 0) && (dy >= 0))
            return (dx * dy) / r2.area();
        return 0;
    }

    private void trackObject(Detection prev) {
        // Take random points in the rect and save them as the previous interest points
        List<Point> previousInterestPointsList = new ArrayList<>();
        for (int i = 0; i < Config.NO_OF_RANDOM_FEATURES_FOR_OPTICAL_FLOW; i++) {
            previousInterestPointsList.add(new Point(prev.getLatest().position.x + new Random().nextInt(prev.getLatest().position.width), prev.getLatest().position.y + new Random().nextInt(prev.getLatest().position.height)));
        }

        MatOfPoint2f previousInterestPoints = new MatOfPoint2f();
        previousInterestPoints.fromList(previousInterestPointsList);
        MatOfPoint2f curInterestPoints = new MatOfPoint2f();
        Video.calcOpticalFlowPyrLK(
                prevFrame,
                curFrame,
                previousInterestPoints,
                curInterestPoints,
                new MatOfByte(),
                new MatOfFloat());

        Vector displacement = getDisplacementOfInterestPoints(previousInterestPoints, curInterestPoints);
        if (Config.SHOULD_DRAW_KLT_FEATURES)
            drawInterestPoints(previousInterestPoints, curInterestPoints, displacement);
        Point tl = new Point(prev.getLatest().position.tl().x + Vector.xComponent(displacement), prev.getLatest().position.tl().y + Vector.yComponent(displacement));
        Point br = new Point(prev.getLatest().position.br().x + Vector.xComponent(displacement), prev.getLatest().position.br().y + Vector.yComponent(displacement));
        Rect newRect = new Rect(tl, br);
        double percentageOfBoundingBoxInRect = percentageOfRect1InRect2(boundingFrame, newRect);

        if (percentageOfBoundingBoxInRect > 0.25) {
            Detection.Location location = new Detection.Location(frameIndex, newRect, false);
            prev.addLocation(location);
        }
    }

    private void drawInterestPoints(MatOfPoint2f prevPoints, MatOfPoint2f curPoints, Vector displacement) {
        openCVWindow.shouldSlowDown = true;
        List<Point> points = prevPoints.toList();
        for (Point point : points) {
            Imgproc.circle(curFrame, point, 2, colorGreen);
        }
        List<Point> cPoints = curPoints.toList();
        for (Point point : cPoints) {
            Imgproc.circle(curFrame, point, 2, colorRed);
        }

        Imgproc.arrowedLine(curFrame, new Point(boundingFrame.width / 2, boundingFrame.height / 2), new Point(boundingFrame.width / 2 + Vector.xComponent(displacement) * 10, boundingFrame.height / 2 + Vector.yComponent(displacement) * 10), colorBlue);
    }

    private Vector getDisplacementOfInterestPoints(MatOfPoint2f previousInterestPoints, MatOfPoint2f curInterestPoints) {
        List<Point> prevPoints = previousInterestPoints.toList();
        List<Point> curPoints = curInterestPoints.toList();
        List<Vector> vectors = new ArrayList<>();

        for (int i = 0; i < prevPoints.size(); i++) {
            Vector vectorOf2Points = Vector.calculateVector(prevPoints.get(i), curPoints.get(i));
            vectors.add(vectorOf2Points);
        }

        return Vector.getResultant(vectors, true);
    }
}