package crearo.algo;

import com.google.common.collect.EvictingQueue;
import org.opencv.core.Rect;

/**
 * Created by rish on 3/1/17.
 */
public class Detection {

    private static final int HISTORY_LENGTH = 25;
    int id;
    EvictingQueue<Location> locations;
    private Location latest;
    private int lastFrameDetectedIn;
    public int totalDetections = 0;

    public Detection(int id, Location location) {
        this.id = id;
        this.locations = EvictingQueue.create(HISTORY_LENGTH);
        addLocation(location);
    }

    public void addLocation(Location location) {
        this.locations.add(location);
        this.latest = location;
        if (location.isDetectedFromSVM) {
            this.lastFrameDetectedIn = location.frameId;
            this.totalDetections++;
        }
    }

    public Location getLatest() {
        return latest;
    }

    public boolean hasBeenTrackedForHistoryLength(int currentFrame) {
        return (currentFrame - this.lastFrameDetectedIn) >= HISTORY_LENGTH;
    }

    static class Location {
        Rect position;
        boolean isDetectedFromSVM;
        int frameId;

        public Location(int frameId, Rect position, boolean isDetectedFromSVM) {
            this.frameId = frameId;
            this.position = position;
            this.isDetectedFromSVM = isDetectedFromSVM;
        }
    }
}
