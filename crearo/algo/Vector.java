package crearo.algo;

import org.opencv.core.Point;

import java.util.List;

/**
 * Created by rish on 4/1/17.
 */
public class Vector {

    double magnitude;
    int angle;

    public Vector() {

    }

    public Vector(double magnitude, int angle) {
        this.magnitude = magnitude;
        this.angle = angle;
    }

    public static double calculateMagnitude(double dx, double dy) {
        return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    }

    public static int calculateAngle(double dx, double dy) {
        return (int) Math.toDegrees(Math.atan2(dy, dx));
    }

    /**
     * From 2 points make a vector with p1 as tail and p2 as head
     *
     * @param p1
     * @param p2
     * @return
     */
    public static Vector calculateVector(Point p1, Point p2) {
        double dx = p2.x - p1.x;
        double dy = p2.y - p1.y;
        return calculateVector(dx, dy);
    }

    public static Vector calculateVector(double dx, double dy) {
        double magnitude = calculateMagnitude(Math.abs(dx), Math.abs(dy));
        int angle = calculateAngle(dx, dy);
        return new Vector(magnitude, angle);
    }

    public static double xComponent(Vector v) {
        return Math.cos(Math.toRadians(v.angle)) * v.magnitude;
    }

    public static double yComponent(Vector v) {
        return Math.sin(Math.toRadians(v.angle)) * v.magnitude;
    }

    /**
     * @param vectors
     * @param shouldOutputAverageVector
     * @return if shouldOutputAverageVector it returns the average magnitude of list of vectors
     */
    public static Vector getResultant(List<Vector> vectors, boolean shouldOutputAverageVector) {
        double dx = 0, dy = 0;
        for (int i = 0; i < vectors.size(); i++) {
            dx += xComponent(vectors.get(i));
            dy += yComponent(vectors.get(i));
        }
        if (shouldOutputAverageVector) {
            dx /= vectors.size();
            dy /= vectors.size();
        }
        return calculateVector(dx, dy);
    }

    @Override
    public String toString() {
        return "Mag:" + this.magnitude + ", Angle:" + this.angle;
    }
}
