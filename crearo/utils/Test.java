package crearo.utils;

import com.google.common.collect.EvictingQueue;
import crearo.algo.Vector;
import org.opencv.core.*;
import org.opencv.ml.Ml;
import org.opencv.ml.SVM;
import org.opencv.ml.TrainData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by rish on 4/1/17.
 */
public class Test {

    public static void main(String[] args) {
        Vector v1 = new Vector(30, 90);
        Vector v2 = new Vector(10, -90);
        List<Vector> list = new ArrayList<>();
        list.add(v1);
        list.add(v2);
        Vector res = Vector.getResultant(list, false);
        Log.d("xComponent :" + Vector.xComponent(res));
        Log.d("yComponent :" + Vector.yComponent(res));
        Log.d("Resultant : " + res.toString());

        Point p1 = new Point(199, 239);
        Point p2 = new Point(184, 260);
        Vector from2Points = Vector.calculateVector(p1, p2);
        Vector from2Differences = Vector.calculateVector(-15, 21);
        Log.d("From2Points : " + from2Points + " from2Differences " + from2Differences.toString());
        Log.d("xComponent :" + Vector.xComponent(from2Points));
        Log.d("yComponent :" + Vector.yComponent(from2Points));
        Log.d("" + Vector.calculateAngle(Vector.xComponent(from2Points), Vector.yComponent(from2Points)));

        EvictingQueue<Integer> queue = EvictingQueue.create(5);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
    }
}
