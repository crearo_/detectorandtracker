package crearo.utils;

/**
 * Created by rish on 4/1/17.
 */
public class Config {

    public static final String YML_FILE = "/home/rish/code/java/projects/detectorandtracker/crearo/res/face_detector32x32.yml";
    public static final String VIDEO_FILE = "/home/rish/code/java/projects/detectorandtracker/crearo/res/Thermal2-Cropped.mp4";
    //    public static final String VIDEO_FILE = "/home/rish/Code/Java/projects/OpenCVStuff/src/crearo/res/Thermal3.avi";
    public static final int NO_OF_RANDOM_FEATURES_FOR_OPTICAL_FLOW = 100; /* Optimal value for this is dependent on size of square/density of features in the square */
    public static final double AREA_THRESHOLD_FOR_RECT_MATCHING = 0.50;
    public static final boolean SHOULD_DRAW_KLT_FEATURES = false;
}
