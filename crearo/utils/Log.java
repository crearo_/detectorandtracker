package crearo.utils;

/**
 * Created by rish on 3/1/17.
 */
public class Log {

    private static boolean shouldLog = false;

    public static void e(String args) {
        if (shouldLog)
            System.err.println(args);
    }

    public static void d(String args) {
        if (shouldLog)
            System.out.println(args);
    }

    public static void wtf(String args) {
        System.err.println(args);
    }
}
