package crearo;

import crearo.utils.Log;
import org.opencv.core.*;
import org.opencv.ml.Ml;
import org.opencv.ml.SVM;
import org.opencv.ml.TrainData;

/**
 * Created by rish on 9/1/17.
 */
public class SvmTraining {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        int labels[] = {-1, 1, -1, 1};
        MatOfInt labelsMat = new MatOfInt();
        for (int i = 0; i < labels.length; i++)
            labelsMat.put(i, 0, labels[i]);

        float trainingData[][] = {{0, 0}, {100, 100}, {20, 20}, {80, 80}};
        MatOfFloat trainingDataMat = new MatOfFloat();
        for (int i = 0; i < trainingData.length; i++) {
            trainingDataMat.put(i, 0, trainingData[i]);
        }

        SVM svm = SVM.create();
        svm.setType(SVM.C_SVC);
        svm.setKernel(SVM.LINEAR);
        svm.setTermCriteria(new TermCriteria(TermCriteria.MAX_ITER, 100, 1e-6));
        svm.train(trainingDataMat, Ml.ROW_SAMPLE, labelsMat);

        float sample[] = {120, 120};
        MatOfFloat samples = new MatOfFloat(1, 2, CvType.CV_32FC1);
        samples.put(1, 2, sample);
        /*
        float predicted = svm.predict(samples);
        Log.d("Predicted label is " + predicted);*/

        Mat results = new Mat();
        svm.predict(samples, results, 2);

        print(labelsMat);
        print(trainingDataMat);
        print(results);
    }

    private static void print(Mat mat) {
        Log.d("Printing a mat!");
        for (int i = 0; i < mat.rows(); i++)
            for (int j = 0; j < mat.cols(); j++) {
                String l = "";
                for (int k = 0; k < mat.get(i, j).length; k++)
                    l += mat.get(i, j)[k] + ",";
                Log.d("" + i + " " + j + " : " + l);
            }
    }
}
