package crearo;

import crearo.algo.DetectorAndTracker;
import crearo.utils.Config;
import crearo.utils.Log;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;

/**
 * Created by rish on 3/1/17.
 * <p>
 * If this doesnt work, use this : https://github.com/opencv-java/getting-started/
 */
public class OpenCVWindow extends JPanel implements MouseListener {

    private BufferedImage image;
    private JFrame jFrame[];

    public static boolean shouldSlowDown = false;
    int totalFrames = 0;
    double currentAverage = 0;

    public static void main(String[] args) throws InterruptedException {
        Log.wtf("Core NATIVE_LIBRARY_NAME is " + Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        OpenCVWindow t = new OpenCVWindow();
        DetectorAndTracker opencv = new DetectorAndTracker(t);
        t.jFrame = new JFrame[5];
        Mat mat = new Mat();
        VideoCapture capture = new VideoCapture(Config.VIDEO_FILE);
        // VideoCapture capture = new VideoCapture(0);

        if (capture.isOpened()) {
            double fps = capture.get(Videoio.CAP_PROP_FPS);
            double h = capture.get(Videoio.CAP_PROP_FRAME_HEIGHT);
            double w = capture.get(Videoio.CAP_PROP_FRAME_WIDTH);

            Log.d("Video Properties : h= " + h + " w= " + w + " fps= " + fps);
            while (true) {
                if (t.shouldSlowDown) {
                    // Thread.sleep(10);
                }
                capture.read(mat);
                if (!mat.empty()) {
                    /* Do CV stuff here */
                    long st = System.currentTimeMillis();
                    // Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
                    opencv.doHog(mat);
                    t.totalFrames++;
                    t.currentAverage = (t.currentAverage * (t.totalFrames - 1) + (System.currentTimeMillis() - st)) / t.totalFrames;
                    Log.d("Avg Time : " + String.format("%.2f", t.currentAverage));

                    /* Display image to jFrame */
                    t.displayMat(mat, 0);
                }
            }
        } else {
            Log.wtf("Isnt opened");
        }
        capture.release();
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }

    public OpenCVWindow() {
    }

    public OpenCVWindow(BufferedImage img) {
        image = img;
    }

    public void displayMat(Mat mat, int id) {
        BufferedImage image = matToBufferedImage(mat);
        if (jFrame[id] == null)
            initWindow(id, image, String.valueOf(id));
        window(id, image);
    }

    private void initWindow(int id, BufferedImage img, String text) {
        jFrame[id] = new JFrame();
        jFrame[id].setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame[id].setTitle(text);
        if (id == 0) {
            jFrame[id].getContentPane().add(new OpenCVWindow(img));
            jFrame[id].setSize(img.getWidth(), img.getHeight());
            jFrame[id].addMouseListener(this);
        } else if (id == 1) {
            jFrame[id].setLayout(new GridLayout());
            jFrame[id].setLocation(jFrame[id - 1].getLocation().x + jFrame[id - 1].getWidth(), 0);
            jFrame[id].setSize(200, 200);
        } else {

        }
        jFrame[id].setVisible(true);
    }

    //Show image on window
    public void window(int id, BufferedImage img) {
        if (id == 0) {
            jFrame[id].getContentPane().removeAll();
            jFrame[id].getContentPane().add(new OpenCVWindow(img));
        } else if (id == 1) {
            jFrame[id].getContentPane().removeAll();
            jFrame[id].getContentPane().add(new OpenCVWindow(img));
        }
        jFrame[id].setVisible(true);
    }

    public BufferedImage matToBufferedImage(Mat frame) {
        // Mat() to BufferedImage
        int type = 0;
        if (frame.channels() == 1) {
            type = BufferedImage.TYPE_BYTE_GRAY;
        } else if (frame.channels() == 3) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        BufferedImage image = new BufferedImage(frame.width(), frame.height(), type);
        WritableRaster raster = image.getRaster();
        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
        byte[] data = dataBuffer.getData();
        frame.get(0, 0, data);

        return image;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        Log.d("Slooooow doooown");
        shouldSlowDown = !shouldSlowDown;
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}