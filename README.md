# Detector And Tracker

Detection based on HOG features fed into SVM classifier.
Tracking is performed only when previously detected frame is not detected again. It is done using KLT, marking feature points of detected objects. 

Trained on thermal images, detection can work given any color space - it is independent of detecting algorithm used.

# Algorithm V1

Store a list of detected objects in previous frame and a list of objects detected in current frame. History is only of size 1.
Also store the previous frame and current frame.

Detect objects in current frame and store them into curFrameObjects. Traverse through each object in prevFrameObjects list and match them with curFrameObjects. This is done by calculating percentage of area covered by prev object on cur object . If the two objects overlap with a certain threshold, match them. ie, if there is an overlap of certain percentage or more between them, that object from the previous frame has been successfully detected again in the current frame. 
If no object in the list curFrameObjects matches a prev object, that object has not been detected in this frame. Hence we must apply tracking. (Hence tracking is only to be done when previously detected object is not detected in subsequent frame.)

For tracking objects, I'm using an optical flow algorithm named KLT. A set of 100 random features from the object to be tracked are marked and sent into KLT. (This number should be changed based on size of detected object. I am not using cornerHarris or maxEigenValues algorithms for feature point detection as finding corners in thermal images is not a good idea).
KLT returns the optical flow of these features/points. I use vector mathematics to find the average displacement of these points. The rect of the object to be tracked is then displaced using this displacement vector. The new rect is then added to curFrameObjects list.

KLT is being applied based on the assumption that objects won't move too fast between each frame. (Current frame rate of video is 25fps).